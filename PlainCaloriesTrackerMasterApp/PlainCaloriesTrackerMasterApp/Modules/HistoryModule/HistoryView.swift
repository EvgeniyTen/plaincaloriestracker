//
//  HistoryView.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 03.08.2023.
//

import SwiftUI

class HistoryViewModel: ObservableObject {
    let core: Core
    
    init(core: Core) {
        self.core = core
    }
}

struct HistoryView: View {
    
    @ObservedObject var viewModel: HistoryViewModel
    
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        HistoryView(viewModel: .init(core: .emptyMock))
    }
}
