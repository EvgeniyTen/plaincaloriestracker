//
//  TrackerView.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 03.08.2023.
//

import SwiftUI
import Combine

class TrackerViewModel: ObservableObject {
    let core: Core
    let textFieldViewModel: TextFieldView.ViewModel
    @Published var products: [ProductRowView.ViewModel] = []
    @Published var isTFVisible: Bool = true

    private var dispose = Set<AnyCancellable>()
    
    init(core: Core) {
        self.core = core
        self.textFieldViewModel = .init(sendAction: core.sendRequest)
        bind()
    }
    
    func bind() {
        core.products
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] entity in
                products = entity
                    .filter {$0.sender != .user}
                    .map(mapCells)
            }
            .store(in: &dispose)
        
        core.isMainTextFieldActive
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] entity in
                isTFVisible = entity
            }
            .store(in: &dispose)
    }
    
    func mapCells(_ input: ProductRow) -> ProductRowView.ViewModel {
        if let data = input.content.data(using: .utf8),
           let product = try? JSONDecoder().decode(ProductModel.self, from: data) {
            return .init(core: core, model: product, rowModel: input)
        } else {
            return .init(core: core, rowModel: input)
        }
    }
}


struct TrackerView: View {
    
    
    @ObservedObject var viewModel: TrackerViewModel
    
    var body: some View {
        ZStack {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVStack {
                    ForEach(viewModel.products) { viewModel in
                        ProductRowView(viewModel: viewModel)
                    }
                    Spacer()
                    textField()
                        .opacity(0)
                }
            }
            textField()
                .opacity(viewModel.isTFVisible ? 1 : 0)
        }

    }
    func textField() -> some View {
        VStack {
            Spacer()
            TextFieldView(viewModel: viewModel.textFieldViewModel)
        }
    }
}

struct TrackerView_Previews: PreviewProvider {
    static var previews: some View {
        TrackerView(viewModel: .init(core: .emptyMock))
    }
}
