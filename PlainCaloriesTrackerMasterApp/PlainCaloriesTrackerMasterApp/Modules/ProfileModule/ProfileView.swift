//
//  ProfileView.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 03.08.2023.
//

import SwiftUI

class ProfileViewModel: ObservableObject {
    let core: Core
    
    init(core: Core) {
        self.core = core
    }
    
    func deleteAll() {
        core.deleteMessages()
    }
}

struct ProfileView: View {
    
    @ObservedObject var viewModel: ProfileViewModel
    
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            .onTapGesture {
                viewModel.deleteAll()
            }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(viewModel: .init(core: .emptyMock))
    }
}
