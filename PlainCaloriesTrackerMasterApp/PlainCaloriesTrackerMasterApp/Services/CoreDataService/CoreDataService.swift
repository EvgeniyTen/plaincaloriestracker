import Foundation
import CoreData
import Combine

enum UpdateType {
    case fav
    case content
    case error
}

class CoreDataService: ObservableObject{
    
    private let date = Date()
    private let container: NSPersistentContainer
    private let containerName: String = "PersistantChatStore"
    private let entityName = "ChatProductsModel"
    private var savedEntities = [ChatProductsModel]()
    
    var savedProducts: CurrentValueSubject<[ProductRow], Never> = .init([])

    init() {
        self.container = NSPersistentContainer(name: containerName)
        container.loadPersistentStores { _, error in
            if let error = error {
                print("Error loading data: \(error.localizedDescription)")
            }
            self.getSavedDevices()
        }
    }
    //MARK: - Public
        
    func add(message: ProductRow) {
        if savedEntities.contains(where: {$0.id == message.id}) {
            guard let index = savedEntities.firstIndex(where: {$0.id == message.id}) else {return}
            savedEntities[index].content = message.content
        } else {
            let entity = ChatProductsModel(context: container.viewContext)
            entity.sender = message.sender.rawValue
            entity.id = message.id
            entity.content = message.content
            entity.createdAt = message.createdAt
            entity.isFav = message.isFav
        }
        
        
        applyChanges()
    }
    
    func update(product: ProductRow, type: UpdateType = .content) {
        savedEntities.forEach { entity in
            if entity.id == product.id {
                switch type {
                case .fav:
                    entity.isFav.toggle()
                case .content:
                    entity.content = product.content
                case .error:
                    entity.content = product.content
                    entity.hasError = true
                }
            }
        }
        applyChanges()
    }
    
    func delete(_ message: ProductRow? = nil) {
        if let index = savedEntities.firstIndex(where: {$0.id == message?.id}) {
            container.viewContext.delete(savedEntities[index])
        } else {
            savedEntities.forEach { message in
                container.viewContext.delete(message)
            }
        }
        
        applyChanges()
    }
    

    //MARK: - Private

    private func applyChanges() {
        save()
        getSavedDevices()
    }
    
    private func save() {
        do {
            try container.viewContext.save()
        } catch let error {
            print("Error saving to Core data: \(error.localizedDescription)")
        }
    }
    
    private func getSavedDevices() {
        let request = NSFetchRequest<ChatProductsModel>(entityName: entityName)
        do {
            savedEntities = try container.viewContext.fetch(request)
            savedProducts.send(savedEntities.map { ProductRow(sender: ChatRole(rawValue: $0.sender ?? "assistant") ?? .assistant ,
                                                              id: $0.id ?? UUID(),
                                                              createdAt: $0.createdAt ?? Date(),
                                                              content: $0.content ?? "Error with reading data",
                                                              isFav: $0.isFav,
                                                              mappingError: $0.hasError
            )})
        } catch let error {
            print("Error fetching Portfolio entity: \(error.localizedDescription)")
        }

    }
}
extension Date {
    func getFormattedDate() -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = "d MMM yyyy"
        return dateformat.string(from: self)
    }
}
