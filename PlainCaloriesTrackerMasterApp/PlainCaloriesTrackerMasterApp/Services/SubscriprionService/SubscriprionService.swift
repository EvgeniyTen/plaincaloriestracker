//
//  SubscriprionService.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 29.06.2023.
//

import Foundation
import StoreKit
import Qonversion
import AppTrackingTransparency
import Combine

struct Product: Identifiable {
    var id = UUID()
    var product: SKProduct
    var price: String
    var period: String
}

enum Products: String, CaseIterable {

    case annual = "year.t"
    case month = "month.t"
    case week = "week.t"

    var period: String {
        switch self {
        case .annual: return "year"
        case .month: return "month"
        case .week: return "week"
        }
    }
}

class QonversionManager {

    static let shared = QonversionManager()

    func getProducts(completion: @escaping ([SKProduct])->Void) {
        Qonversion.shared().offerings { offerings, Error in

            if let offers = offerings?.availableOfferings.first {
            var products: [SKProduct] = []

                for pr in offers.products {
                    if let skProduct = pr.skProduct {
                        products.append(skProduct)
                    }
                }
            completion(products)
            } else {
                completion([])
            }
        }
    }

    func buy(product: SKProduct, purchaseCompletion: @escaping (Bool)->Void) {
        Qonversion.shared().purchase(product.productIdentifier) { (permissions, error, isCancelled) in
            if let premium = permissions["premium"], premium.isActive {
                purchaseCompletion(true)
            } else {
                purchaseCompletion(false)
            }
        }
    }
    

    func status(completion: @escaping (Bool)->Void) {
        Qonversion.shared().checkEntitlements { (permissions, error) in
            if let premium = permissions["premium"], premium.isActive {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func restore(completion: @escaping (Bool) -> Void) {
        Qonversion.shared().restore {(permissions, error) in
            if let error = error {
                print("error here 4 \(String(describing: error.localizedDescription))")
                completion(false)
                return
            }
            guard permissions.values.contains(where: {$0.isActive == true}) else {
                completion(false)
                return
                
            }
            if let permission: Qonversion.Entitlement = permissions["premium"], permission.isActive {
                completion(true)
                return
            }
            
        }
    }

    func price(product: SKProduct) -> String {
        let price = product.price

        if price == NSDecimalNumber(decimal: 0.00) {
            return ""
        } else {
            let numberFormatter = NumberFormatter()
            let locale = product.priceLocale
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = locale
            return numberFormatter.string(from: price) ?? ""
        }
    }

    func period(product: SKProduct) -> String {
        return (Products.allCases.filter { $0.rawValue == product.productIdentifier }).first?.period ?? ""
    }
}

class SubscriprionService: ObservableObject {
    
    
    private lazy var userService = UserService()
    
    
    var products: CurrentValueSubject<[Product], Never> = .init([])
    var isPremium: CurrentValueSubject<Bool, Never> = .init(false)
    
    init() {
        let config = Qonversion.Configuration(projectKey: "qonverion api key", launchMode: .subscriptionManagement)
        Qonversion.shared().setProperty(.adjustAdID, value: userService.uuid())
        Qonversion.initWithConfig(config)
        
                
        QonversionManager.shared.getProducts(completion: { sKproducts in
            print("sKproducts \(sKproducts.count)")
//            self.products.send(sKproducts
//                .map({Product(product: $0,
//                              price: QonversionManager.shared.price(product: $0),
//                              period: QonversionManager.shared.period(product: $0))}))
            
        })
        
        self.products.send([Product(product: .init(), price: "$ 4,99", period: "week"),
                            Product(product: .init(), price: "$ 7,99", period: "month"),
                            Product(product: .init(), price: "$ 9,99", period: "year")])
    }

    func quickBuy(group: DispatchGroup, product: Product) {
        QonversionManager.shared.buy(product: product.product, purchaseCompletion: { [weak self] isPremium in
            self?.isPremium.send(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                group.leave()
            }
        })

    }
    
    func isPurchased(group: DispatchGroup) {
        QonversionManager.shared.status(completion: { [weak self] isPremium in
            self?.isPremium.send(isPremium)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                group.leave()
            }
        })
    }
    
    func restore(group: DispatchGroup) {
        QonversionManager.shared.restore(completion: { [weak self] isPremium in
            self?.isPremium.send(isPremium)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                group.leave()
            }        })
    }
    
}
