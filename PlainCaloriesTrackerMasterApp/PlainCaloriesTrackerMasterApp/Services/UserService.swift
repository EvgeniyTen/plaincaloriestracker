//
//  UserService.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 29.06.2023.
//

import Foundation

private extension String {
    static let uuidUserDefaultsKey = "uuidKey"
}

final class UserService {
    
    private let userDefaults = UserDefaults.standard
    
    func uuid() -> String {
        if let uuid = userDefaults.value(forKey: .uuidUserDefaultsKey) as? String {
            return uuid
        }
        
        let uuid = NSUUID().uuidString.lowercased()
        userDefaults.set(uuid, forKey: .uuidUserDefaultsKey)
        return uuid
    }
}
