//
//  ProductRow.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 05.08.2023.
//

import Foundation


struct ProductModel: Codable {
    let product: String
    let fats,carbon,protein,calories,primaryWeight: Double?
}

struct ProductRow {
    let sender: ChatRole
    let id: UUID
    let createdAt: Date
    var content: String
    var isFav: Bool
    private(set) var mappingError: Bool = false
}
