//
//  Color+Extensions.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 13.05.2023.
//

import SwiftUI

extension Color {
    
    static func random()->Color {
      let r = Double.random(in: 0 ... 1)
      let g = Double.random(in: 0 ... 1)
      let b = Double.random(in: 0 ... 1)
      return Color(red: r, green: g, blue: b)
    }
    
    //Text Colors
    static let textBlack = Color("TextBlack")
    static let textGrey = Color("TextGrey")
    static let keyboardGrey = Color("KeyboardGrey")
    static let textWhite = Color("TextWhite")

    //Button Colors
    static let buttonBlack = Color("ButtonBlack")
    static let buttonGrey = Color("ButtonGrey")
    static let buttonWhite = Color("ButtonWhite")

    //Icons colors
    static let iconBlack = Color("IconBlack")
    static let iconGrey = Color("IconGrey")
    static let iconLightGrey = Color("IconLightGrey")
    static let iconWhite = Color("IconWhite")

    //Theme colors
    static let themeBlue = Color("ThemeBlue")
    static let themeGreen = Color("ThemeGreen")
    static let themeNavyGreen = Color("ThemeNavyGreen")
    static let themeOrange = Color("ThemeOrange")
    static let themePink = Color("ThemePink")
    static let themeViolet = Color("ThemeViolet")
    static let themeYellow = Color("ThemeYellow")

    static let greenGradient = [Color.themeNavyGreen, Color.themeGreen]
}

