//
//  View+Extensions.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 13.05.2023.
//

import SwiftUI

extension View {
    
    var screenSize: CGRect {
        UIScreen.main.bounds
    }
    
    func screenWidth(by value: Double) -> CGFloat {
        UIScreen.main.bounds.width * value
    }
    
    func screenHeight(by value: Double) -> CGFloat {
        UIScreen.main.bounds.height * value
    }
    
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
    
    func inExpandingRectangle() -> some View {
         ZStack {
             Rectangle()
                 .fill(Color.clear)
             self
         }
     }
    
}

extension View {

    public func popup<PopupContent: View>(
        isPresented: Binding<Bool>,
        isPopUp: Bool,
        onDismiss: @escaping () -> Void,
        view: @escaping () -> PopupContent) -> some View {
        self.modifier(
            Popup(
                isPresented: isPresented,
                isPopUp: isPopUp,
                onDismiss: onDismiss,
                view: view)
        )
    }
}

extension View {
    func onBackSwipe(perform action: @escaping () -> Void) -> some View {
        gesture(
            DragGesture()
                .onEnded({ value in
                    if value.startLocation.x < 50 && value.translation.width > 80 {
                        action()
                    }
                })
        )
    }
}

extension View {
    func frameGetter(_ frame: Binding<CGRect>) -> some View {
        modifier(FrameGetter(frame: frame))
    }
}
  
struct FrameGetter: ViewModifier {
  
    @Binding var frame: CGRect
    
    func body(content: Content) -> some View {
        content
            .background(
                GeometryReader { proxy -> AnyView in
                    let rect = proxy.frame(in: .global)
                    // This avoids an infinite layout loop
                    if rect.integral != self.frame.integral {
DispatchQueue.main.async {
                            self.frame = rect
                        }
                       }
                return AnyView(EmptyView())
            })
    }
}
