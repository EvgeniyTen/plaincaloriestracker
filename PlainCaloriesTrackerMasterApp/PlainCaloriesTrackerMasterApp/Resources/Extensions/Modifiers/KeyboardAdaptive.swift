//
//  KeyboardAdaptive.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 11.07.2023.
//

import SwiftUI
import Combine

/// Note that the `KeyboardAdaptive` modifier wraps your view in a `GeometryReader`,
/// which attempts to fill all the available space, potentially increasing content view size.
struct KeyboardAdaptive: ViewModifier {
    @State private var bottomPadding: CGFloat = 0
    @State var isActive: Bool
    
    func body(content: Content) -> some View {
        if isActive {
            content
                .padding(.bottom, self.bottomPadding)
                .frame(maxHeight: .infinity, alignment: .bottom)
                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                    let keyboardTop = UIScreen.main.bounds.height - keyboardHeight
                    let focusedTextInputBottom = UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0
                    //magic number - is padding inset of textFieldFrame
                    self.bottomPadding = max(0, focusedTextInputBottom - keyboardTop + 30)
                }
                .animation(.linear, value: bottomPadding)
        } else {
            content
        }
    }
}

extension View {
    func keyboardAdaptive(isActive: Bool) -> some View {
        ModifiedContent(content: self, modifier: KeyboardAdaptive(isActive: isActive))
    }
}
