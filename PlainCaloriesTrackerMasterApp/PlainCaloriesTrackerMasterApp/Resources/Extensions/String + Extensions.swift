//
//  String + Extensions.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 29.06.2023.
//

import Foundation

extension String {
    
    func priceToCurrency() -> String {
        let numbers = "0123456789,. "
        return self.filter({!numbers.contains($0)})
    }
    
    func priceToDouble() -> Double {
        let comma = ","
        let dot = "."
        let numbers = "0123456789"
        let filterString = comma + dot + numbers
        let numString = self.filter { filterString.contains($0) }.replacingOccurrences(of: ",", with: ".")
            
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current

        let formattedString = formatter.number(from: numString)
        
        if let price = formattedString?.doubleValue {
            return price
        } else if let price = Double(numString) {
            return price
        } else {
            return 0
        }
    }
}
