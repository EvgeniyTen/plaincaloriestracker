//
//  Font+Extension.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 13.05.2023.
//

import SwiftUI

extension Font {
    
    // Headline
    static func systemHeadLine1() -> Font {
        return Font.system(size: 33, weight: .bold)
    }
    static func systemHeadLine2() -> Font {
        return Font.system(size: 22, weight: .semibold)
    }
    static func systemHeadLine3() -> Font {
        return Font.system(size: 18, weight: .medium)
    }
    static func systemHeadLine4() -> Font {
        return Font.system(size: 28, weight: .bold)
    }
    
    // Subtitle
    static func systemSubtitle1() -> Font {
        return Font.system(size: 17, weight: .regular)
    }
    static func systemSubtitle2() -> Font {
        return Font.system(size: 17, weight: .semibold)
    }
    
    // Body
    static func systemBody1() -> Font {
        return Font.system(size: 15, weight: .semibold)
    }
    static func systemBody2() -> Font {
        return Font.system(size: 15, weight: .regular)
    }
    static func systemBody3() -> Font {
        return Font.system(size: 13, weight: .medium)
    }
    
    // Overline
    static func systemOverline() -> Font {
        return Font.system(size: 12, weight: .bold)
    }
}
