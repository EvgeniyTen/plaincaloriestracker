//
//  Float + Extension.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 18.05.2023.
//

import Foundation

extension Float {
    func truncate(places : Int)-> Float {
        return Float(floor(pow(10.0, Float(places)) * self)/pow(10.0, Float(places)))
    }
}

extension Double {
    func truncate(places : Int)-> String {
        return removeZerosFromEnd(Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places))))
    }
    
    func removeZerosFromEnd(_ value: Double) -> String {
            let formatter = NumberFormatter()
            let number = NSNumber(value: self)
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 16 //maximum digits in Double after dot (maximum precision)
            return String(formatter.string(from: number) ?? "")
        }
}
