//
//  Date + Extensions.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 06.06.2023.
//

import Foundation

extension Date {
   func formattedString() -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = "d MMM"
        return dateformat.string(from: self)
    }
}
