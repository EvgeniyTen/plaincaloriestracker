//
//  PopupView.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 29.06.2023.
//

import SwiftUI

public struct Popup<PopupContent>: ViewModifier where PopupContent: View {
    
    init(isPresented: Binding<Bool>,
         isPopUp: Bool = true,
         onDismiss: @escaping () -> Void,
         view: @escaping () -> PopupContent) {
        self._isPresented = isPresented
        self.isPopUp = isPopUp
        self.onDismiss = onDismiss
        self.view = view
    }
    
    @Binding var isPresented: Bool
    @State var isPopUp: Bool
    var view: () -> PopupContent
    var onDismiss: () -> Void
    @State private var presenterContentRect: CGRect = .zero
    @State private var sheetContentRect: CGRect = .zero
    
    private var displayedOffset: CGFloat {
        if isPopUp {
           return  -presenterContentRect.midY + screenHeight/2
        } else {
            return presenterContentRect.height - screenHeight / 2
        }
    }
    private var hiddenOffset: CGFloat {
        if presenterContentRect.isEmpty {
            return 1000
        }
        if isPopUp {
            return screenHeight - presenterContentRect.midY + sheetContentRect.height/2 + 5
        } else {
            return screenHeight - sheetContentRect.height/2
        }
    }
    private var currentOffset: CGFloat {
        return isPresented ? displayedOffset : hiddenOffset
    }
    private var screenWidth: CGFloat {
        UIScreen.main.bounds.size.width
    }
    private var screenHeight: CGFloat {
        UIScreen.main.bounds.size.height
    }

    public func body(content: Content) -> some View {
        ZStack {
            content
                .frameGetter($presenterContentRect)
        }
        .overlay(sheet())
    }

    func sheet() -> some View {
        ZStack {
            
            self.view()
              .simultaneousGesture(
                  TapGesture().onEnded {
                      dismiss()
              })
              .frameGetter($sheetContentRect)
              .frame(width: screenWidth)
              .offset(x: 0, y: currentOffset)
              .animation(.spring(), value: currentOffset)
        }
    }

    private func dismiss() {
        isPresented = false
        onDismiss()
    }
}
