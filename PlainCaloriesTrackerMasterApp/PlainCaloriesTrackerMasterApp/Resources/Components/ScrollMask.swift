//
//  ScrollMask.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 06.06.2023.
//

import SwiftUI

struct ScrollMask: View {
    let isTop: Bool

    var body: some View {
        LinearGradient(colors: [.black, .clear],
                       startPoint: UnitPoint(x: 0.5, y: isTop ? 0 : 1),
                       endPoint: UnitPoint(x: 0.5, y: isTop ? 1 : 0))
            .frame(height: 40)
            .frame(maxWidth: .infinity)
            .blendMode(.destinationOut)

    }
}
