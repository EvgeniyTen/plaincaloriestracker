//
//  NeiroPopupView.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 29.06.2023.
//

import SwiftUI

struct NeiroPopupView: View {
    var answers: Int
    var action: () -> Void
    var body: some View {
        VStack(spacing: 10) {
            HStack {
                Spacer()
                Image("close.cross")
            }
            Image("proShieldCross")
                .resizable()
                .frame(width: 25, height: 25)
                .padding(10)
                .background {
                    Circle()
                        .foregroundColor(.buttonBlack)
                }
            Text("You have \( 5 - answers) free answers")
                .foregroundColor(.textBlack)
                .font(.systemHeadLine2())
            
            Text("To increase the limit, subscribe.")
                .foregroundColor(.textBlack)
                .font(.systemSubtitle1())
                .frame(maxWidth: .infinity)
                .lineLimit(1)
            NeiroButtonView(viewModel: .init(style: .dark, title: "Go to PRO", widhtByScreen: 0.85, action: action))
                .padding(.top, 20)
            
        }
        .padding()
        .background {
            RoundedRectangle(cornerRadius: 20)
                .foregroundColor(.buttonGrey)
                .shadow(color: .textGrey.opacity(0.4), radius: 5, y: 2)

        }
    }
}

struct NeiroPopupView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.white
            NeiroPopupView(answers: 4, action: {})
                .padding(.horizontal)
        }
    }
}
