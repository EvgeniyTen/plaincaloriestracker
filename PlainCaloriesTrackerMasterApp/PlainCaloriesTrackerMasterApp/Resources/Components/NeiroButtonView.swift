//
//  NeiroButtonView.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 06.06.2023.
//

import SwiftUI

enum ButtonColorScheme {
    case dark
    case light
}

extension NeiroButtonView {
    class ViewModel: ObservableObject {
        var title: String
        var font: Font
        var titleColor: Color
        var image: Image?
        var backgroundColor: Color
        var heightByScreen: Double?
        var widhtByScreen: Double?
        var action: () -> Void
        
        init(title: String, font: Font, titleColor: Color, image: Image? = nil, backgroundColor: Color, heightByScreen: Double? = 0.1, widhtByScreen: Double? = 0.9, action: @escaping () -> Void) {
            self.title = title
            self.font = font
            self.titleColor = titleColor
            self.image = image
            self.backgroundColor = backgroundColor
            self.heightByScreen = heightByScreen
            self.widhtByScreen = widhtByScreen
            self.action = action
        }
        
        convenience init(style: ButtonColorScheme, title: String, image: Image? = nil, heightByScreen: Double? = 0.08, widhtByScreen: Double? = 0.9, action: @escaping() -> Void ) {
            switch style {
            case .dark:
                self.init(title: title, font: .systemSubtitle2(), titleColor: .textWhite, image: image, backgroundColor: .buttonBlack, heightByScreen: heightByScreen, widhtByScreen: widhtByScreen, action: action)
            case .light:
                self.init(title: title, font: .systemBody1(), titleColor: .textBlack, image: image, backgroundColor: .buttonGrey, heightByScreen: heightByScreen, widhtByScreen: widhtByScreen, action: action)
            }
        }
        
    }
}

struct NeiroButtonView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        Button {
            viewModel.action()
        } label: {
            HStack {
                viewModel.image?
                    .renderingMode(.template)
                    .foregroundColor(.black)
                Text(viewModel.title)
                    .foregroundColor(viewModel.titleColor)
                    .font(viewModel.font)
                    
            }
            .frame(width: screenWidth(by: viewModel.widhtByScreen ?? 0.9), height: screenHeight(by: viewModel.heightByScreen ?? 0.08))
                .background {
                    RoundedRectangle(cornerRadius: 15)
                        .foregroundColor(viewModel.backgroundColor)
            }
        }

        
    }
}

struct NeiroButtonView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            NeiroButtonView(viewModel: .init(style: .dark, title: "Go to PRO", action: {}))
            NeiroButtonView(viewModel: .init(style: .light, title: "View Prompts", image: Image("lightbulbOther"), heightByScreen: 0.06, widhtByScreen: 0.5, action: {}))

        }
    }
}
