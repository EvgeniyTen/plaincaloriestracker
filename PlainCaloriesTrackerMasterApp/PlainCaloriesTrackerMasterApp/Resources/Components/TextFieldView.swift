//
//  TextField.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 04.08.2023.
//

import SwiftUI

extension TextFieldView {
    class ViewModel: ObservableObject {
        
        @Published var currentText: String = ""
        @Published var isSubMenuHidden: Bool = true
        @Published var keyboardHidden: Bool = true
        @Published var textFieldActive: Bool = false
        
        var sendAction: (String) -> Void
        
        init(sendAction: @escaping (String) -> Void) {
            self.sendAction = sendAction
        }
        
        func hideSubmenu(completion: @escaping ()->Void) {
            withAnimation(.spring()) {
                self.isSubMenuHidden = true
                completion()
            }
        }
        
    }
}
struct TextFieldView: View {
    
    @ObservedObject var viewModel: ViewModel
    var body: some View {
        ZStack(alignment: .bottom) {
            //MARK: -- Place to import submenu
            VStack(spacing: 0) {
                HStack {
                    TextField("Enter to calculate...", text: $viewModel.currentText) { state in
                        withAnimation(.spring(response: 0.6)) {
                            viewModel.textFieldActive = state
                            viewModel.keyboardHidden = !state
                        }
                    }
                    .font(.systemBody1())
                    .padding([.top, .bottom, .leading], 30)
                    Button {
                        send()
                    } label: {
                        Image(systemName: "paperplane.circle")
                            .renderingMode(.template)
                            .padding(5)
                            .opacity(viewModel.currentText == "" ? 0.5 : 1 )
                            .allowsHitTesting(viewModel.currentText != "")
                    }
                    Spacer()
                        .frame(width: 25)
                }
                
                .background (
                    Rectangle()
                        .cornerRadius(10, corners: [.allCorners])
                        .padding()
                    
                        .foregroundColor(viewModel.keyboardHidden ? .iconLightGrey : .white)
                        .background(
                            Rectangle()
                            
                                .cornerRadius(10, corners: viewModel.keyboardHidden ? [.allCorners] : [])
                                .foregroundColor(viewModel.keyboardHidden ? .clear : .keyboardGrey)
                            
                        )
                )
            }
            .padding(.top, 10)
            .onChange(of: viewModel.textFieldActive, perform: { newValue in
                withAnimation(.easeInOut) {
                    if !viewModel.keyboardHidden {
                        viewModel.isSubMenuHidden = newValue
                    }
                }
            })
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .foregroundColor(viewModel.isSubMenuHidden ? . clear : .buttonGrey)
                
            )
        }
    }
    
    private func send() {
        UIApplication.shared.endEditing()
        
        withAnimation(.easeInOut) {
            if viewModel.currentText.trimmingCharacters(in: .whitespacesAndNewlines).count > 1 {
                viewModel.sendAction(viewModel.currentText)
                viewModel.currentText = ""
                viewModel.keyboardHidden = true
            }
        }
    }
}

struct ChatTextFieldView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
            TextFieldView(viewModel: .init(sendAction: {_ in}))
        }
    }
}
