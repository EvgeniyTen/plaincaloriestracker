//
//  ProductRowView.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 05.08.2023.
//

import SwiftUI

extension ProductRowView {
    
    class ViewModel: ObservableObject, Identifiable {
        @Published var model: ProductModel?
        @Published var rowModel: ProductRow
        @Published var additionalMenuVisible = false
        
        private let core: Core
        let shortcutsViewModel: AdditionalSection.ViewModel

        init(core: Core,
             model: ProductModel? = nil,
             rowModel: ProductRow,
             additionalMenuHidden: Bool,
             shortcutsViewModel: AdditionalSection.ViewModel
             
        ) {
            self.core = core
            self.model = model
            self.rowModel = rowModel
            self.additionalMenuVisible = additionalMenuHidden
            self.shortcutsViewModel = shortcutsViewModel
        }
        
        convenience init(core: Core, model: ProductModel, rowModel: ProductRow) {
            self.init(core: core,
                      model: model,
                      rowModel: rowModel,
                      additionalMenuHidden: false,
                      shortcutsViewModel: .init(core: core))
        }
        
        convenience init(core: Core, rowModel: ProductRow) {
            self.init(core: core,
                      rowModel: rowModel,
                      additionalMenuHidden: rowModel.mappingError,
                      shortcutsViewModel: .init(core: core))
        }
        
        func deleteRow(completion: @escaping () -> Void) {
            core.deleteMessage(by: rowModel.id)
            completion()
        }
    }
}


struct ProductRowView: View {
    @ObservedObject var viewModel: ViewModel
    @State var deleteButtonVisible: Bool = false
    var body: some View {
        
        LazyVStack {
            ZStack {
                deleteButton
                rowContent
                    .onTapGesture {
                        if !deleteButtonVisible {
                            viewModel.additionalMenuVisible.toggle()
                        } else {
                            deleteButtonVisible = false
                        }
                    }
                    .offset(x: deleteButtonVisible ? -30 : 0)
            }
            .padding(.vertical, 5)
            .gesture(DragGesture(minimumDistance: 100, coordinateSpace: .local)
                                .onEnded(simulateGesture))
            additionalMenu
        }
        .animation(.spring(), value: viewModel.additionalMenuVisible)
        .animation(.spring(), value: deleteButtonVisible)

        .padding(.horizontal, 20)
        
    }
    
    func simulateGesture(value: DragGesture.Value) {
        
        
        if value.translation.width < 0{
            // left
            if !deleteButtonVisible {
                deleteButtonVisible = true
            }
        }
        if value.translation.width > 0 {
            // right
            if deleteButtonVisible {
                deleteButtonVisible = false
            }
        }
    }

}

struct ProductRowView_Previews: PreviewProvider {
    static var previews: some View {
        ProductRowView(viewModel: .init(core: .emptyMock, model: .init(product: "shdgi", fats: 1.0235, carbon: 1.0235, protein: 1.0235, calories: 1, primaryWeight: 1), rowModel: .init(sender: .assistant, id: UUID(), createdAt: Date(), content: "DFHGJKS", isFav: false)))
    }
}

extension ProductRowView {
    
    @ViewBuilder
    var additionalMenu: some View {
        if viewModel.additionalMenuVisible {
            AdditionalSection(viewModel: viewModel.shortcutsViewModel)
                .background {
                    RoundedRectangle(cornerRadius: 10)
                        .foregroundColor(.white)
                        .shadow(radius: 2)
                }
                .opacity(viewModel.additionalMenuVisible ? 1 : 0)
        } else {
            EmptyView()
        }
    }
    
    var deleteButton: some View {
        HStack {
            Spacer()
            Button {
                viewModel.deleteRow {
                    viewModel.additionalMenuVisible = false
                    deleteButtonVisible.toggle()
                }
                
            } label: {
                Image(systemName: "xmark")
            }

        }
    }
    func dataRow(_ model: ProductModel) -> some View {
        VStack {
            HStack {
                Text(model.product.capitalized)
                    .padding(.horizontal)
                    .frame(height: 40)
                    .background {
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(style: .init(lineWidth: 1))
                            .foregroundColor(.themeBlue)
                    }
                Spacer()

            }
            HStack {
                HStack {
                    Text("Белки")
                    Spacer()
                    Text(model.protein?.truncate(places: 2) ?? "")
                        .frame(width: screenWidth(by: 0.22), height: 40)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .fill(Color.iconGrey)
                        }
                }
                HStack {
                    Text("Жиры")
                    Spacer()
                    Text(model.fats?.truncate(places: 2) ?? "")
                        .frame(width: screenWidth(by: 0.22), height: 40)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .fill(Color.iconGrey)
                        }

                }
            }
            HStack {
                HStack {
                    Text("Углеводы")
                    Spacer()
                    Text(model.carbon?.truncate(places: 2) ?? "")
                        .frame(width: screenWidth(by: 0.22), height: 40)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .fill(Color.iconGrey)
                        }
                }
                HStack {
                    Text("Калории")
                    Spacer()
                    Text(model.calories?.truncate(places: 2) ?? "")
                        .frame(width: screenWidth(by: 0.22), height: 40)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .fill(Color.iconGrey)
                        }

                }
            }
        }
    }
    
    @ViewBuilder
    var rowContent: some View {
        if let model = viewModel.model {
            dataRow(model)
        } else {
            HStack {
                Text(viewModel.rowModel.content)
                    .font(.systemBody2())
                    .foregroundColor(.textBlack)
                Spacer()
            }
            .padding()
            .background {
                RoundedRectangle(cornerRadius: 10)
                    .foregroundColor(.white)
                    .shadow(radius: 2)
            }
        }
        
       
        
    }
}
