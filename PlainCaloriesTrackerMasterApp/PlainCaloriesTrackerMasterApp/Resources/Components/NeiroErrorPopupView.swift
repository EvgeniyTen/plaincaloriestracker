//
//  NeiroErrorPopupView.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 10.07.2023.
//

import SwiftUI
extension NeiroErrorPopupView {
    class ViewModel: ObservableObject {
        var regenerateAction: () -> Void
        var deleteAction: () -> Void

        init(regenerateAction: @escaping () -> Void, deleteAction: @escaping () -> Void) {
            self.regenerateAction = regenerateAction
            self.deleteAction = deleteAction
        }
    }
}
struct NeiroErrorPopupView: View {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack(spacing: 10) {
            Button {
                viewModel.regenerateAction()
            } label: {
                Text("Try again")
                    .foregroundColor(.textGrey)
                    .font(.systemSubtitle2())
                    .frame(width: screenWidth(by: 0.95), height:70)
                    .background {
                        RoundedRectangle(cornerRadius: 20)
                            .foregroundColor(.buttonWhite)
                    }
            }
            .padding(.top, 20)

            
            Button {
                viewModel.deleteAction()
            } label: {
                Text("Delete a message")
                    .foregroundColor(.red)
                    .font(.systemSubtitle2())
                    .frame(width: screenWidth(by: 0.95), height:70)

                    .background {
                        RoundedRectangle(cornerRadius: 20)
                            .foregroundColor(.buttonWhite)
                    }
            }

        }
        .frame(width: screenWidth(by: 1), height: screenHeight(by: 0.3), alignment: .top)
        .background {
            Rectangle()
                .cornerRadius(20, corners: [.topLeft, .topRight])
                .foregroundColor(.buttonGrey)
        }
        
        
    }
}

struct NeiroErrorPopupView_Previews: PreviewProvider {
    static var previews: some View {
        NeiroErrorPopupView(viewModel: .init(regenerateAction: {}, deleteAction: {}))
    }
}
