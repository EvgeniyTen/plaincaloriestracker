//
//  NavigationBarComponent.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 18.05.2023.
//

import SwiftUI

extension NavigationBarView {
    
    class ViewModel: ObservableObject {
        
        @Published var title: Title
        @Published var leftButtons: [BaseButtonViewModel]
        @Published var rightButtons: [BaseButtonViewModel]
        @Published var textColor: Color
        @Published var backgroundColor: Color
        @Published var state: State
        
        
        internal init(title: Title,
                      leftButtons: [BaseButtonViewModel] = [],
                      rightButtons: [BaseButtonViewModel] = [],
                      textColor: Color = .iconBlack,
                      state: State = .normal
        ) {
            
            self.title = title
            self.leftButtons = leftButtons
            self.rightButtons = rightButtons
            self.textColor = textColor
            self.backgroundColor = .white
            self.state = state
        }
        
        enum State {
            
            case normal
            case hidden
        }
        
        enum Title {
            
            case text(String)
            case empty
        }
        
        class BaseButtonViewModel: ObservableObject ,Identifiable {
            
            let id: UUID = UUID()
        }
        
        class ButtonViewModel: BaseButtonViewModel {
            
            @Published var icon: Image
            @Published var action: () -> Void
            
            init(icon: Image, action: @escaping () -> Void) {
                
                self.icon = icon
                self.action = action
                super.init()
            }
        }
        
        class TextButtonViewModel: BaseButtonViewModel {
            
            @Published var icon: Image
            @Published var text: String
            @Published var action: () -> Void
            
            init(icon: Image, text: String, action: @escaping () -> Void) {
                
                self.icon = icon
                self.text = text
                self.action = action
                super.init()
            }
        }
        
        class BackButtonViewModel: BaseButtonViewModel {
            
            @Published var icon: Image
            @Published var action: () -> Void
            
            init(icon: Image = Image("navBarBackButton"), action: @escaping () -> Void) {
                
                self.icon = icon
                self.action = action
                super.init()
            }
        }
    }
}

struct NavigationBarView: View {

    @ObservedObject var viewModel: ViewModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var leftPlaceholdersCount: Int {
        
        return max(viewModel.rightButtons.count - viewModel.leftButtons.count, 0)
    }
    
    var rightPlaceholdersCount: Int {
        
        return max(viewModel.leftButtons.count - viewModel.rightButtons.count, 0)
    }
    
    var body: some View {
        
        switch viewModel.state {
            
        case .normal:
            
            VStack {
                HStack(alignment: .center, spacing: 0) {
                    
                    HStack(alignment: .top, spacing: 18) {
                        
                        ForEach(viewModel.leftButtons) { button in
                            
                            switch button {
                            case let backButtonViewModel as NavigationBarView.ViewModel.BackButtonViewModel:
                                Button {
                                    
                                    mode.wrappedValue.dismiss()
                                    backButtonViewModel.action()
                                    
                                } label: {
                                    backButtonViewModel.icon
                                        .resizable()
                                        .frame(width: 30, height: 30, alignment: .leading)
                                        .foregroundColor(.buttonBlack)
                                }
                                
                            case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                                Button(action: buttonViewModel.action){
                                    
                                    buttonViewModel.icon
                                        .foregroundColor(.buttonBlack)

                                }
                            default:
                                EmptyView()
                            }
                        }
                        
                        ForEach(0..<leftPlaceholdersCount, id: \.self) { _ in
                            Spacer().frame(width: 20, height: 20)
                        }
                    }
                    
                    Spacer()
                    
                    switch viewModel.title {

                    case .text(let title):
                        Text(title)
                            .font(.systemBody1())
                            .foregroundColor(viewModel.textColor)
                            .lineLimit(1)
                        
                    case .empty:
                        EmptyView()
                    }
                    
                    Spacer()
                    
                    HStack(alignment: .top, spacing: 18) {
                        
                        ForEach(0..<rightPlaceholdersCount, id: \.self) { _ in
                            Spacer().frame(width: 20, height: 20)
                        }
                        
                        ForEach(viewModel.rightButtons) { button in
                            
                            switch button {
                            case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                                Button(action: buttonViewModel.action){
                                    
                                    buttonViewModel.icon
                                }
                                
                            case let buttonViewModel as NavigationBarView.ViewModel.TextButtonViewModel:
                                Button(action: buttonViewModel.action){
                                    HStack(spacing: 5) {
                                        buttonViewModel.icon
                                        if buttonViewModel.text == "5/5" {
                                            Text("Go to")
                                                .font(.systemOverline())
                                                .foregroundColor(.textWhite)
                                            Text("PRO")
                                                .font(.systemOverline())
                                                .foregroundStyle(
                                                    .linearGradient(
                                                        colors: Color.greenGradient,
                                                        startPoint: .top,
                                                        endPoint: .bottom
                                                    )
                                                )
                                        } else {
                                            Text(buttonViewModel.text)
                                                .font(.systemOverline())
                                                .foregroundStyle(
                                                    .linearGradient(
                                                        colors: Color.greenGradient,
                                                        startPoint: .top,
                                                        endPoint: .bottom
                                                    )
                                                )
                                            Text("Free")
                                                .font(.systemOverline())
                                                .foregroundColor(.textWhite)
                                        }
                                            
                                    }
                                    .padding(10)
                                    .background(
                                        RoundedRectangle(cornerRadius: 13)
                                            .foregroundColor(.textBlack)
                                        )
                                    
                                }
                            default:
                                EmptyView()
                            }
                        }
                    }
                }
               
            }
            .padding(.horizontal, 14)
            .frame(height: 70)
            .background(
                Color.accentColor.edgesIgnoringSafeArea(.top)
        )
            
        case .hidden:
            
            HStack(alignment: .center, spacing: 4) {
                
                ForEach(viewModel.leftButtons) { button in
                    
                    switch button {
                    case let backButtonViewModel as NavigationBarView.ViewModel.BackButtonViewModel:
                        
                        Button {
                            
                            mode.wrappedValue.dismiss()
                            backButtonViewModel.action()
                            
                        } label: {
                            
                            ZStack {
                                
                                Circle()
                                    .frame(width: 40, height: 40)
                                    .foregroundColor(.white)
                                
                                backButtonViewModel.icon
                                    
                            }
                        }
                        
                    case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                        
                        Button(action: buttonViewModel.action){
                            
                            ZStack {
                                
                                Circle()
                                    .frame(width: 36, height: 36)
                                    .foregroundColor(.white)
                                
                                buttonViewModel.icon
                                   
                            }
                        }
                    default:
                        EmptyView()
                    }
                }
            }
            .padding(.leading, 8)
            .padding(.top, 12)

        }
    }

}

struct NavigationBarViewModifier: ViewModifier {
    
    @State var viewModel: NavigationBarView.ViewModel
    
    func body(content: Content) -> some View {
        
        switch viewModel.state {
            
        case .normal:
            
            ZStack(alignment: .topLeading) {

                content
                    .navigationBarHidden(true)

                NavigationBarView(viewModel: viewModel)
            }
            
        case .hidden:
            
            ZStack(alignment: .topLeading) {
                
                content
                    .navigationBarHidden(true)

                NavigationBarView(viewModel: viewModel)

            }
        }
    }
}

extension View {
    
    func navigationBar(with viewModel: NavigationBarView.ViewModel) -> some View {
        
        self.modifier(NavigationBarViewModifier(viewModel: viewModel))
    }
}

extension NavigationBarView.ViewModel {
    
    static let sample0 = NavigationBarView.ViewModel(title: .empty,
                                                     leftButtons: [NavigationBarView.ViewModel.BackButtonViewModel(action: {})],
                                                     rightButtons: [NavigationBarView.ViewModel.TextButtonViewModel(icon: Image("proShieldCross"), text: " 5 /5" , action: {})])
static let sample1 = NavigationBarView.ViewModel(title: .empty,
    leftButtons: [NavigationBarView.ViewModel.BackButtonViewModel(action: {})],
    rightButtons: [NavigationBarView.ViewModel.TextButtonViewModel(icon: Image("proShieldCross"), text: " 5 /5" , action: {})])
}

struct NavigationBarView_Previews: PreviewProvider {
    
    static var previews: some View {

        Group {
            ScrollView {
                VStack(spacing: 10) {
                    Color.green
                        .frame(height: 180)
                    Color.orange
                        .frame(height: 180)
                }
                .padding(.top, 70)
            }
            .navigationBar(with: .sample0)
            ScrollView {
                VStack(spacing: 10) {
                    Color.green
                        .frame(height: 180)
                    Color.orange
                        .frame(height: 180)
                }
                .padding(.top, 70)
            }
            .navigationBar(with: .sample1)
        }
    }
}
