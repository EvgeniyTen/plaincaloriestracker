
import SwiftUI

struct AdditionalModel {
    var title: String
    var image: Image {
        switch title {
        case "Weight":
            return Image(systemName: "scalemass")
        case "Amount":
            return Image(systemName: "takeoutbag.and.cup.and.straw")
        default :
            return Image(systemName: "birthday.cake")
        }
    }
    var subsctring: String {
        switch title {
        case "Weight":
            return "1000gr..."
        case "Amount":
            return "2 or 0.5..."
        default :
            return ""
        }
    }
}

enum AdditionalType {
    case weight(AdditionalModel)
    case amount(AdditionalModel)
}

extension AdditionalButtonWithTextfield {
    class ViewModel: ObservableObject {
        private let type: AdditionalType
        private let core: Core
        
        var title: String
        var image: Image
        var action: (String) -> Void
        
        @Published var tapped: Bool = false
        @Published var currentText: String = ""
        @Published var textFieldPLaceholder = ""
        init(core: Core, type: AdditionalType, action: @escaping (String) -> Void) {
            self.core = core
            self.type = type
            switch type {
            case .weight(let additionalModel):
                self.title = additionalModel.title
                self.textFieldPLaceholder = additionalModel.subsctring
                self.image = additionalModel.image

            case .amount(let additionalModel):
                self.title = additionalModel.title
                self.textFieldPLaceholder = additionalModel.subsctring
                self.image = additionalModel.image
            
            }
            self.action = action
            self.tapped = tapped
        }
        
        func updateMainTextField(state: Bool) {
            core.updateTextFieldState(state)
        }
    }
}

struct AdditionalButtonWithTextfield: View {
    
    @ObservedObject var viewModel: ViewModel

    var body: some View {
        if viewModel.tapped {
            HStack {
                viewModel.image

                ZStack {
                    TextField(viewModel.textFieldPLaceholder, text: $viewModel.currentText) { state in
                        viewModel.updateMainTextField(state: !state)
                    }
                    .font(.systemBody1())
                    .keyboardType(.numberPad)
                    .padding(.trailing, 30)

                    Button {
                        withAnimation(.easeInOut) {
                            viewModel.tapped = false
                            viewModel.action(viewModel.currentText)
                        }
                    } label: {
                        Image(systemName: "checkmark.circle.fill")
                    }
                    .frame(maxWidth: .infinity, alignment: .trailing)

                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .frame(height: 20)
            .padding(15)
            .background {
                RoundedRectangle(cornerRadius: 10)
                    .foregroundColor(.buttonWhite)
            }
            
        } else {
            HStack {
                viewModel.image
                Text(viewModel.currentText == "" ? viewModel.title : viewModel.title + ": " + viewModel.currentText)
                    .font(.systemBody1())
                    .foregroundColor(.textBlack)
                
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .frame(height: 20)
            .padding(15)
            .background {
                RoundedRectangle(cornerRadius: 10)
                    .foregroundColor(.buttonWhite)
            }
            .onTapGesture {
                withAnimation(.easeInOut) {
                    viewModel.tapped = true
                }
            }
        }
    }
}

struct NeiroShortcutsButton_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
            AdditionalButtonWithTextfield(viewModel: .init(core: .emptyMock, type: .weight(.init(title: "Amount")), action: {_ in}))
        }
    }
}
