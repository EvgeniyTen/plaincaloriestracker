//
//  LoaderView.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 04.07.2023.
//

import SwiftUI

struct LoaderView: View {
    var body: some View {
        VStack {
            Spacer()
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
                .scaleEffect(2)
            Spacer()
        }
    }
}

struct LoaderView_Previews: PreviewProvider {
    static var previews: some View {
        LoaderView()
    }
}
