//
//  NeiroNetworkErrorView.swift
//  neuroChat
//
//  Created by Evgeniy Timofeev on 13.07.2023.
//

import SwiftUI

extension NeiroNetworkErrorView {
    class ViewModel: ObservableObject {
        let action: () -> Void
        
        init(action: @escaping () -> Void) {
            self.action = action
        }
    }
}

struct NeiroNetworkErrorView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        ZStack {
            Color.buttonWhite.ignoresSafeArea()
            VStack(spacing: 30) {
                Image("cryingLogo")
                VStack(alignment: .center, spacing: 10) {
                    Text("Something went wrong because\nthere is no internet connection...")
                        .foregroundColor(.textBlack)
                        .font(.systemBody1())
                        .multilineTextAlignment(.center)

                    Text("Try to check your internet connection\nin phone settings")
                        .foregroundColor(.textGrey)
                        .font(.systemBody2())
                    .multilineTextAlignment(.center)
                }
                Button {
                    viewModel.action()
                } label: {
                    Text("Try again")
                        .foregroundColor(.textWhite)
                        .font(.systemBody1())
                        .padding()
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .foregroundColor(.textGrey)
                        }
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

struct NeiroNetworkErrorView_Previews: PreviewProvider {
    static var previews: some View {
        NeiroNetworkErrorView(viewModel: .init(action: {}))
    }
}
