//
//  CustomTabbarPreferenceKey.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 14.05.2023.
//

import Foundation
import SwiftUI

struct CustomTabBarItemPreferenceKey: PreferenceKey {
     
    static var defaultValue: [TabBarItem] = []
    
    static func reduce(value: inout [TabBarItem], nextValue: () -> [TabBarItem]) {
        value += nextValue()
    }
}

struct CustomTabBarItemViewModifier: ViewModifier {
    
    let tab: TabBarItem
    @Binding var selection: TabBarItem
    
    func body(content: Content) -> some View {
        content
            .opacity(selection == tab ? 1.0 : 0)
            .preference(key: CustomTabBarItemPreferenceKey.self, value: [tab])
    }
}

extension View {
    func tabBarItem(tab: TabBarItem, selection: Binding<TabBarItem>) -> some View {
      modifier(CustomTabBarItemViewModifier(tab: tab, selection: selection))
    }
}
