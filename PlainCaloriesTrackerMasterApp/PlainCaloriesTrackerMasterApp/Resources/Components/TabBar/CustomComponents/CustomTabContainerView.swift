//
//  CustomTabContainerView.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 14.05.2023.
//

import SwiftUI

enum TabBarItem: Hashable {
    case tracker, history, profile
    
    var title: String {
        switch self{
        case .tracker: return "Tracker"
        case .history: return "History"
        case .profile: return "Profile"
            
        }
    }
    
    var image: String {
        switch self{
        case .tracker: return "fork.knife.circle"
        case .history: return "book.circle"
        case .profile: return "person.circle"
            
        }
    }
}

struct CustomTabContainerView<Content:View>: View {
    
    let content: Content
    @Binding var selection: TabBarItem
    @State var tabs: [TabBarItem] = []
    @Binding var tabbarIsHidden: Bool
    
    init(selection: Binding<TabBarItem>, tabBarIsHidden: Binding<Bool> ,@ViewBuilder _ content: () -> Content) {
        self._selection = selection
        self._tabbarIsHidden = tabBarIsHidden
        self.content = content()
    }
    
    var body: some View {
        VStack {
            ZStack {
                content
            }
            if !tabbarIsHidden {
                CustomTabBarView(tabs: tabs, selected: $selection, tabbarIsHidden: $tabbarIsHidden)

            }
        }
        .onPreferenceChange(CustomTabBarItemPreferenceKey.self) { tabs in
            self.tabs = tabs
        }
        .animation(.spring(), value: selection)
    }
}

struct CustomTabContainerView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabContainerView(selection: .constant(.tracker), tabBarIsHidden: .constant(false)) {
        }
    }
}
