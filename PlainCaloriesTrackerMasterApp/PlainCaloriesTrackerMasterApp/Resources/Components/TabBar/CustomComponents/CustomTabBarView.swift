//
//  CustomTabbarView.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 14.05.2023.
//

import SwiftUI

struct CustomTabBarView: View {
    
    let tabs: [TabBarItem]
    
    @Namespace var namespace
    @Binding var selected: TabBarItem
    @Binding var tabbarIsHidden: Bool

    var body: some View {
        HStack(alignment: .center) {
            Spacer()
            ForEach(tabs, id: \.self) { tab in
                tabView(tab: tab)
                    .onTapGesture {
                        switchToTab(tab: tab)
                    }
            }
            .padding([.horizontal, .vertical], 5)
            Spacer()

        }
        .opacity(tabbarIsHidden ? 0 : 1)
        .animation(.easeInOut, value: tabbarIsHidden)
    }
}

struct CustomTabBarView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.white
            CustomTabBarView(tabs: [.tracker, .history, .profile], selected: .constant(.tracker), tabbarIsHidden: .constant(false))
        }
    }
}

extension CustomTabBarView {
    private func tabView(tab: TabBarItem) -> some View {
        HStack(alignment: .center) {
            Image(systemName: tab.image)
            if selected == tab {
                Text(tab.title)
                    .font(.systemBody1())
            }
        }
        .padding([.horizontal, .vertical])
        .foregroundColor(selected == tab ? .iconBlack : .iconGrey)
        .background(
            RoundedRectangle(cornerRadius: 10)
                .foregroundColor(selected == tab ? .white : .clear)
                .shadow(color: .black.opacity(0.2) ,radius: 3, y: 3)
        )
    }
    
    private func switchToTab(tab: TabBarItem) {
        withAnimation(.linear) {
            selected = tab
        }
    }
}

