//
//  TabBarView.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 13.05.2023.
//

import SwiftUI
import Combine

struct TabBarView: View {
        
    @ObservedObject var viewModel: TabBarViewModel
    
    var body: some View {
            CustomTabContainerView(selection: $viewModel.selectedTab, tabBarIsHidden: $viewModel.isHidden){
                
                NavigationView {
                    TrackerView(viewModel: viewModel.trackerModule)
                }
                .navigationBarHidden(true)
                .tabBarItem(tab: .tracker,
                                selection: $viewModel.selectedTab)
                
                NavigationView {
                    HistoryView(viewModel: viewModel.historyModule)
                }
                .navigationViewStyle(.stack)
                .navigationBarHidden(true)
                .tabBarItem(tab: .history,
                                selection: $viewModel.selectedTab)
                
                
                NavigationView {
                    ProfileView(viewModel: viewModel.profileModule)
                }
                .navigationBarHidden(true)
                .tabBarItem(tab: .profile,
                                selection: $viewModel.selectedTab)
                
            }
            .ignoresSafeArea(.keyboard, edges: .bottom)
    }
}

struct MainTabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView(viewModel: .init(core: .emptyMock))
    }
}

