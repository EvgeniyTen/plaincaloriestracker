//
//  TabBarViewModel.swift
//  MegaCleaner
//
//  Created by Evgeniy Timofeev on 13.05.2023.
//

import SwiftUI
import Combine

class TabBarViewModel: ObservableObject {
    
    @Published var selectedTab: TabBarItem = .tracker
    @Published var isHidden: Bool = false
    
    @Published var cartBageCount = 0
    @Published var favoritesBageCount = 0
    
    let trackerModule: TrackerViewModel
    let historyModule: HistoryViewModel
    let profileModule: ProfileViewModel
    
    let core: Core
    
    private var dispose = Set<AnyCancellable>()
    
    internal init(core: Core) {
        self.core = core
        self.trackerModule = .init(core: core)
        self.historyModule = .init(core: core)
        self.profileModule = .init(core: core)
        
        bind()
    }
    
    func bind() {
        
        $selectedTab
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] selectedTab in
                core.action.send(TabBarAction.CloseAll())
            }
            .store(in: &dispose)
        
        core.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as TabBarAction.ChangeTab:
                    selectedTab = payload.selectedTab
                    
                case let payload as TabBarAction.BarVisible:
                    isHidden = payload.hidden
                    
                default:
                    break
                }
            }
            .store(in: &dispose)
    }
}

//MARK: - TabBar Actions
enum TabBarAction {
    
    struct ChangeTab: Action {
        
        let selectedTab: TabBarItem
    }
    
    struct BarVisible: Action {
        let hidden: Bool
    }
    
    struct CloseAll: Action { }
    
}
