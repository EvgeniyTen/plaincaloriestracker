//
//  Shortcuts.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 04.08.2023.
//

import SwiftUI

extension AdditionalSection {
    class ViewModel: ObservableObject {
        var weightBlock: AdditionalButtonWithTextfield.ViewModel
        var amountBlock: AdditionalButtonWithTextfield.ViewModel

        @Published var isPromptsVisible: Bool = false

        init(weightBlock: AdditionalButtonWithTextfield.ViewModel,
             amountBlock: AdditionalButtonWithTextfield.ViewModel) {
            self.weightBlock = weightBlock
            self.amountBlock = amountBlock
        }
        
        convenience init(core: Core) {
            self.init(weightBlock: .init(core: core, type: .weight(.init(title: "Weight")), action: core.updateWeight),
                      amountBlock: .init(core: core, type: .weight(.init(title: "Amount")), action: core.updateAmount))
        }
    }
}

struct AdditionalSection: View {
    
    @ObservedObject var viewModel: ViewModel
    var body: some View {
        HStack(alignment: .center) {
            AdditionalButtonWithTextfield(viewModel: viewModel.weightBlock)
            AdditionalButtonWithTextfield(viewModel: viewModel.amountBlock)
        }
        .opacity(viewModel.isPromptsVisible ? 0 : 1)
        .padding(.horizontal)
        .onAppear {
            viewModel.isPromptsVisible = false
        }
    }
}

struct NeiroShortcuts_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.blue
            AdditionalSection(viewModel: .init(core: .emptyMock))
        }
    }
}

