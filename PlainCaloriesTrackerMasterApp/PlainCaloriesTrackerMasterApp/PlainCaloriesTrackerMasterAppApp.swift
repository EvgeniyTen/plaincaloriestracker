//
//  PlainCaloriesTrackerMasterAppApp.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 03.08.2023.
//

import SwiftUI

@main
struct PlainCaloriesTrackerMasterAppApp: App {
    
    private let core: Core = {
        let openAIService = OpenAIService()
        let persistent = CoreDataService()
        let subscriptionService = SubscriprionService()
        return Core(openAIService: openAIService, persistent: persistent, subscriptionService: subscriptionService)
    }()
    
    var body: some Scene {
        WindowGroup {
            ContentView(core: core)
        }
    }
}
