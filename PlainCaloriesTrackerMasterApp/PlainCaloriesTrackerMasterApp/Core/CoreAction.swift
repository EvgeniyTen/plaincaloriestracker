
import Foundation

enum CoreAction {}

extension CoreAction {
    
    struct AddProductToFav: Action {
        let value: UUID
    }
    
    struct ShowFavs: Action {
        let value: [ProductRow]
    }
    
    struct HideFavs: Action {}
}

extension CoreAction {
    enum NavigationActions {
        
        struct ShowGuide: Action {}
        
        struct ShowFeedback: Action {}
        
        struct ShowSettings: Action {}
        
        struct ShowCharts: Action {}
        
    }

    
    enum OpenAIAction{
        struct SendRequestWith: Action {
            let value: String
        }
    }
}

extension CoreAction {
    enum SubscriptionService {
        struct Buy: Action {
            let group: DispatchGroup
            let product: Product
        }
        
        struct Restore: Action {
            let group: DispatchGroup
        }
    }
}

