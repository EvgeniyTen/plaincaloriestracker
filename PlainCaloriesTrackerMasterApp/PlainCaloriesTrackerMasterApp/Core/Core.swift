import SwiftUI
import Combine

class Core {
    
    let action: PassthroughSubject<Action, Never> = .init()
    var isPurchased: CurrentValueSubject<Bool, Never> = .init(false)
    var products: CurrentValueSubject<[ProductRow], Never> = .init([])
    var isProAlertHidden: CurrentValueSubject<Bool, Never> = .init(true)
    var isMainTextFieldActive: CurrentValueSubject<Bool, Never> = .init(true)
    var subscriptionProducts: CurrentValueSubject<[Product], Never> = .init([])
    var connectionError: CurrentValueSubject<Bool, Never> = .init(false)
    
    private let openAIService: OpenAIService
    private let persistent: CoreDataService
    private let subscriptionService: SubscriprionService
    private var dispose = Set<AnyCancellable>()
    private var primaryWeight = ""
    private var amountOfProduct = ""

    
    init(openAIService: OpenAIService,
         persistent: CoreDataService,
         subscriptionService: SubscriprionService) {
        self.openAIService = openAIService
        self.persistent = persistent
        self.subscriptionService = subscriptionService
        bind()
    }

    //MARK: Public actions
    
    func backAction() {
        action.send(TabBarAction.BarVisible(hidden: false))
    }

    func updateWeight(_ value: String) {
        primaryWeight = value + "грамм"
        regenerate()
    }
    
    func updateAmount(_ value: String) {
        amountOfProduct = "В количестве" + value + "штук"
        regenerate()
    }
    
    func updateTextFieldState(_ state: Bool) {
        isMainTextFieldActive.send(state)
    }
    
    func showFavs(_ group: DispatchGroup) {
        withAnimation(.easeInOut) {
            if products.value.isEmpty {
                // TODO: показать алерт
            } else {
                let filteredMessages = products.value.filter{ $0.isFav }
                action.send(TabBarAction.BarVisible.init(hidden: true))
                action.send(CoreAction.ShowFavs(value: filteredMessages))
            }
            group.leave()
        }
    }
    
    func hideFavs(_ group: DispatchGroup) {
        withAnimation(.easeInOut) {
            products.send(products.value)
            action.send(TabBarAction.BarVisible.init(hidden: false))
            action.send(CoreAction.HideFavs())
            group.leave()
        }
    }
    
    func updateProAlertVisibility() {
        isProAlertHidden.value.toggle()
    }
    
    func pushToGuide() {
        action.send(TabBarAction.BarVisible.init(hidden: true))
        action.send(CoreAction.NavigationActions.ShowGuide.init())
    }
    
    
    func sendRequest(with value: String) {
        let product = ProductRow(sender: .user, id: UUID(), createdAt: .now, content: value, isFav: false)
        send(product)
    }
    
    func updateMessage(with id: UUID) {
        if let index = products.value.firstIndex(where: {$0.id == id}) {
            persistent.update(product: products.value[index])
        }
        
    }

    //MARK: Private actions
    
    private func bind() {
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                case let payload as CoreAction.OpenAIAction.SendRequestWith:
                    sendRequest(with: payload.value)
                    
                case let payload as CoreAction.AddProductToFav:
                    updateMessage(with: payload.value)
                    
                case let payload as CoreAction.SubscriptionService.Buy:
                    subscriptionService.quickBuy(group: payload.group, product: payload.product)
                    
                case let payload as CoreAction.SubscriptionService.Restore:
                    subscriptionService.restore(group: payload.group)
                    
                default:
                    break
                }
            }
            .store(in: &dispose)
        
        persistent.savedProducts
            .receive(on: DispatchQueue.main)
            .sink { [unowned self]  entity in
                products.send(entity)
            }
            .store(in: &dispose)
        
        subscriptionService.products
            .receive(on: DispatchQueue.main)
            .sink { [unowned self]  entity in
                subscriptionProducts.send(entity)
            }
            .store(in: &dispose)
        
        subscriptionService.isPremium
            .receive(on: DispatchQueue.main)
            .sink { [unowned self]  entity in
                isPurchased.send(entity)
            }
            .store(in: &dispose)
    }
    
    func checkNetworkAvailability() {
        connectionError.send(!Reachability.shared.isConnectedToNetwork())
    }
    
    func regenerate() {
        if Reachability.shared.isConnectedToNetwork() {
            if let lastUserMessage = products.value.last(where: {$0.sender == .user})?.content,
               let lastAssistantMessage = products.value.last(where: {$0.sender == .assistant})
            {
                deleteMessage(by: lastAssistantMessage.id)
                action.send(CoreAction.OpenAIAction.SendRequestWith(value: lastUserMessage))
            }
        } else {
            connectionError.send(true)
        }
        
    }
    
    func deleteMessage(by id: UUID) {
        let product = products.value.last(where: { $0.id == id })
        persistent.delete(product)
    }
    
    func deleteMessages() {
        persistent.delete()
    }

    private func send(_ message: ProductRow) {
        persistent.add(message: message)
        Task {
            try await Task.sleep(nanoseconds: 1000)
            var answer = ProductRow(sender: .assistant, id: UUID(), createdAt: .now, content: "", isFav: false)
            persistent.add(message: answer)

            do {
                let additionalData = "Мне нужна пищевая ценность этого продукта \(primaryWeight) \(amountOfProduct)? пришли ответ только в формате json product: String, fats: Double, carbon : Double, protein : Double, calories : Double, primaryWeight : Double или комментарий о том, что тебе не хватает указанного веса порции для расчета, если это продукт питания и любую шутку, если это не продукт питания"
                let response = try await openAIService.sendMessage(message.content + additionalData).trimmingCharacters(in: .whitespacesAndNewlines)
                answer.content = response
                persistent.update(product: answer, type: .content)
                
            } catch {
                answer.content = error.localizedDescription
                persistent.update(product: answer, type: .error)
            }
        }
    }
}


extension Core {
    
    static var emptyMock: Core {
        let openAIService = OpenAIService()
        let persistent = CoreDataService()
        let subscriptionService = SubscriprionService()
        return Core(openAIService: openAIService, persistent: persistent, subscriptionService: subscriptionService)
    }
}
