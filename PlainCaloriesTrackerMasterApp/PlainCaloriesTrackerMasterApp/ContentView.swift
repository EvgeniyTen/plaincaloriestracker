//
//  ContentView.swift
//  PlainCaloriesTrackerMasterApp
//
//  Created by Evgeniy Timofeev on 03.08.2023.
//

import SwiftUI


struct ContentView: View {
    
    private let core: Core
    
    init(core: Core) {
        self.core = core
    }
    
    var body: some View {
        TabBarView(viewModel: .init(core: core))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(core: .emptyMock)
    }
}
